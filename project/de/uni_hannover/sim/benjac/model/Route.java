package de.uni_hannover.sim.benjac.model;
import de.uni_hannover.sim.benjac.simulation.*;
/**
*Route, die Autos nehmen, um von Kreuzung zu Kreuzung zu navigieren. Eine einfach verkettete
* Liste mit Kreuzung als aktuellem Segment und Verweis auf nächsten Knoten.
*@author benjac
*@version 1.0
*/
public class Route {
        /**
        *Aktuelle Kreuzung, die verfügbar ist.
        *
        */
        private Kreuzung segment;
        /**
        *Verweis auf nächsten Listenknoten.
        *
        */
        private Route next;

        public Kreuzung getSegment(){ return this.segment;}
        public void setSegment(Kreuzung segment){ this.segment = segment;}

        public Route getNext(){ return this.next;}
        public void setNext(Route next){ this.next = next;}
        /**
        *Konstruktor für Route
        *
        */
        public Route(Kreuzung segment){
            this.segment = segment;
        }
}
