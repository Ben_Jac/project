package de.uni_hannover.sim.benjac.model;
import de.uni_hannover.sim.benjac.simulation.*;
import java.util.concurrent.ThreadLocalRandom;

/**
* Unterklasse von Kreuzung. Stellt Systemgrenzen des Strassennetzes dar. Enhält eine spawn Probability, ein Array
* von Routen für Autos und eine Update-Methode mit Helfer-Methoden.
* @author benjac
* @version 1.0
*/
public class Border extends Kreuzung{
    /**
    * Die Wahrscheinlichkeit, dass pro Aufruf der Update-Methode Autos gespawnt
    * werden.
    */
    private int spawnProb = 0;

    /**
    * Ein Array an Routen, die in Update an Autos vergeben werden.
    */
    private Route[] routes = new Route[2];

    public int getSpawnProb(){ return this.spawnProb;}
    public void setSpawnProb(int spawnProb){ this.spawnProb = spawnProb;}

    public Route[] getRoutes(){ return this.routes;}
    public void setRoutes(Route[] routes){ this.routes = routes;}
    /**
    * Konstruktor für Systemgrenzen
    * @param n Der Name dieser Grenze als String.
    *
    */
    public Border(String n){
        super(n);
        super.name = n;
    }

    public void logStatus(){

    }
    public double[] getLog(){

        double[] d = new double[2];
        return d;
    }
    /**
    * Helfer-Methode für Update()
    * @return integer Der Index, an welcher Stelle im Array sich freier Platz befindet.
    * @param actors Das Array, das untersucht wird.
    */
    public static int freeSpaceInArr(Actor[] actors){
        int index = -1;
        for(int k = 0; k<actors.length-1; k++){
            if(actors[k] == null) return k;
        }
        return index;
    }

    /**
    * Update-Methode für Systemgrenzen. Spawnt neue Autos mit einer gewissen Wahrscheinlichkeit,
    * wenn Platz im Array des Strassennetzes frei ist. Weist den neuen Autos zufällig
    * eine Route zu.
    * @return nothing
    * @param net Das Strassennetz, das bearbeitet wird.
    */
    public void update(Strassennetz net){
        java.util.Random rand;
        if(net.getSeed() != -1) {
            rand = new java.util.Random(net.getSeed());
            net.setSeed(net.getSeed()+1);
        } else {
            rand = new java.util.Random();
        }
        int randomNum1 = rand.nextInt(101);
        int randomNum2 = rand.nextInt(3);



        int spawnIndex = -1;
        for(int j = 0; j<super.out.length; j++){
            if(super.out[j] != null) spawnIndex = j;
        }
        if(spawnIndex != -1 && randomNum1 < this.spawnProb && super.out[spawnIndex].getFields()[0] == false){
            int CarIndex = freeSpaceInArr(net.getActors());
            if( CarIndex != -1){
                Auto auto = new Auto(super.out[spawnIndex], 0, 0);
                auto.setRoute(this.routes[randomNum2]);
                net.setActor(CarIndex, auto);
                super.out[spawnIndex].setFields(0, true);

            }

        }
    }
}
