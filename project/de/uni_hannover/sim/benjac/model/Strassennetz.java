package de.uni_hannover.sim.benjac.model;
import de.uni_hannover.sim.benjac.simulation.*;
/**
* Diese Klasse enthält das Netz an Straßen, Kreuzungen und Autos. Es werden
* Kreuzungen und Autos als Akteure betrachtet.
* @version 1.0
* @author benjac
*/
public class Strassennetz{
    /**
    *Ein Array an Akteuren, die im Netz aktiv sind.
    */
    private Actor[] actors = new Actor[50];

    /**
    *Ein Array an Straßen, die im Netz verfügbar sind.
    */
    private Fahrbahn[] streets = new Fahrbahn[8];

    private double wayCount = 0;
    private int carsSoFar = 0;
    private long seed = -1;

    public Actor getActor(int index){ return this.actors[index];}
    public Actor[] getActors(){ return this.actors;}
    public void setActors(Actor[] actors){ this.actors = actors;}
    public void setActor(int index, Actor actor){ this.actors[index] = actor;}

    public Fahrbahn getStreet(int index){ return this.streets[index];}
    public Fahrbahn[] getStreets(){ return this.streets;}
    public void setStreets(Fahrbahn[] streets){ this.streets = streets;}
    public void setStreet(int index,Fahrbahn street){ this.streets[index] = street;}

    public double getWayCount(){ return this.wayCount;}
    public void setWayCount(double wayCount){ this.wayCount = wayCount;}

    public int getCarsSoFar(){ return this.carsSoFar;}
    public void setCarsSoFar(int value){ this.carsSoFar = value;}

    public long getSeed(){ return this.seed;}
    public void setSeed(long seed){ this.seed = seed;}
    /**
    *Konstruktor für neues Strassennetz.
    */
    public Strassennetz(Kreuzung[] crossings, Fahrbahn n, Fahrbahn e, Fahrbahn s, Fahrbahn w, Fahrbahn n2, Fahrbahn e2, Fahrbahn s2, Fahrbahn w2){
        for(int i = 0; i<crossings.length; i++){
            this.actors[i] = crossings[i];
        }

        streets[0] = n;
        streets[1] = e;
        streets[2] = s;
        streets[3] = w;
        streets[4] = n2;
        streets[5] = e2;
        streets[6] = s2;
        streets[7] = w2;
    }
}
