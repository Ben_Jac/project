package de.uni_hannover.sim.benjac.model;
import de.uni_hannover.sim.benjac.simulation.*;
/**
*Unterklasse von Kreuzung. Stellt Kreuzungen mit Ampeln dar. Enthält die verbleibende Zeit
*bis zur nächsten Ampelphase und den Index der aktuellen Ampelphase.
*@author benjac
*@version 1.0
*/
public class LightJunction extends Kreuzung{
    /**
    *Verbleibende Zeit bis zur nächsten Ampelphase
    *
    */
    private int remainTime = 3;
    /**
    *Index der aktuellen Ampelphase
    *
    */
    private int phaseIndex;

    public int getRemainTime(){ return this.remainTime;}
    public void setRemainTime(int value){ this.remainTime = value;}

    public int getPhaseIndex(){ return this.phaseIndex;}
    public void setPhaseIndex(int value){ this.phaseIndex = value;}
    /**
    *Konstruktor für Ampelkreuzungen
    *
    */
    public LightJunction (String n){
        super(n);
        this.name = n;
    }

    private double[] log = new double[100];
    private int logIndex = 0;

    public void setLog(double[] log){ this.log = log;}

    public void logStatus(){
        this.log[this.logIndex] = countWaitingCars();
        this.logIndex++;
    }

    public double[] getLog(){
        return this.log;
    }

    double countWaitingCars(){
        double result = 0.0;
        for(int j = 0; j<this.in.length; j++){
            if(this.in[j].getGreenPhase() == false){
                for(int n = this.in[j].getLength()-1; n>0; n--){
                    if(this.in[j].getFields()[n] == true){
                        result += 1.0;
                        if(this.in[j].getFields()[n-1] == false) break;
                    }
                }
            }
        }
        return result;
    }
    /**
    *Update-Methode für Ampelkreuzungen. Sie schaltet die Ampeln der umliegendende
    * Straßen auf grün oder rot abhängig von der aktuellen Ampelphase und verbleibenden
    *Zeit.
    */
    public void update(Strassennetz net){
        if(this.remainTime == 0){
            switch(this.phaseIndex){
                case 0: if(this.in[0] != null) this.in[0].setGreenPhase(true);
                        if(this.in[2] != null) this.in[2].setGreenPhase(true);
                        if(this.in[1] != null) this.in[1].setGreenPhase(false);
                        if(this.in[3] != null) this.in[3].setGreenPhase(false);
                        this.remainTime = 3;
                        this.phaseIndex = 1;

                        break;
                case 1: if(this.in[0] != null) this.in[0].setGreenPhase(false);
                        if(this.in[2] != null) this.in[2].setGreenPhase(false);
                        if(this.in[1] != null) this.in[1].setGreenPhase(true);
                        if(this.in[3] != null) this.in[3].setGreenPhase(true);
                        this.remainTime = 3;
                        this.phaseIndex = 0;

                        break;
            }
        }
        this.remainTime--;

    }
}
