package de.uni_hannover.sim.benjac.model;
import de.uni_hannover.sim.benjac.simulation.*;
/**
* Eine abstrakte Gerüst-Klasse für Kreuzungen und Autos.
* @author benjac
* @version 1.0
*/
public abstract class Actor {
    /**
    *Ein abstraktes Gerüst für Update-Methoden der Unterklassen.
    *@return nothing
    *@param net Das Traßennetz, das bearbeitet wird.
    */
    public abstract void update(Strassennetz net);

    public abstract void logStatus();

}
