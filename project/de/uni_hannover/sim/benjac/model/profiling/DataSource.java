package de.uni_hannover.sim.benjac.model.profiling;

public interface DataSource{
    public void logStatus();
    public double[] getLog();
}
