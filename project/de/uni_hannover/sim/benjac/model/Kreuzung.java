package de.uni_hannover.sim.benjac.model;
import de.uni_hannover.sim.benjac.simulation.*;
import de.uni_hannover.sim.benjac.model.profiling.*;
/**
*Kreuzung ist eine Unterklasse von der abstrakten Klasse Akteur. Sie enthält ihren Namen,
* und Arrays für ein- und ausgehende Straßen.
*@author benjac
*@version 1.0
*/
public abstract class Kreuzung extends Actor implements DataSource{
    protected String name;

    /**
    *Ein Array aus Straßen, die zur Kreuzung führen.
    */
    protected Fahrbahn[] in = new Fahrbahn[4];
    /**
    *Ein Array aus Straßen, die von der Kreuzung ausgehen.
    */
    protected Fahrbahn[] out = new Fahrbahn[4];

    public String getName(){ return this.name;}
    public void setName(String name){ this.name = name;}

    public Fahrbahn[] getIn(){ return this.in;}
    public void setIn(int index,Fahrbahn street){ this.in[index] = street;}

    public Fahrbahn[] getOut(){ return this.out;}
    public void setOut(int index,Fahrbahn street){ this.out[index] = street;}



    /**
    *Konstruktor für Kreuzung
    *
    */
    public Kreuzung(String n){
        this.name = n;
    }

    public abstract void logStatus();
    public abstract double[] getLog();

}
