package de.uni_hannover.sim.benjac.model;
import de.uni_hannover.sim.benjac.simulation.*;
import de.uni_hannover.sim.benjac.model.profiling.*;

/**
Ein Auto-Objekt. Diese KLasse beeinhaltet eine Straße, einen Index auf das aktuelle Feld auf
dieser Straße, einen Zähler, der die Lebenszeit der Instanz hochzählt, eine Route, die
das Auto nehmen soll und eine Update-Methode.
@author benjac
@version 1.0
*/

public class Auto extends Actor implements DataSource{
    /**
    *Referenz auf die Straße, auf der das Auto sich aktuell bewegt.
    */
    private Fahrbahn street;
    /**
    *Ganzzahl, die das aktuelle Feld auf der Straße angibt.
    */
    private int field = 0;
    /**
    *Zähler, der die aktuelle Lebenszeit der Instanz des Autos angibt.
    */
    private int lifeTime = 0;
    /**
    *Route, die das Auto nehmen wird.
    */
    private double[] log = new double[50];
    private int logIndex = 0;
    private int fieldsThisRound;
    private Route route;

    public Fahrbahn getStreet(){ return this.street;}
    public int getField(){ return this.field;}
    public int getLifeTime(){ return this.lifeTime;}
    public int getFieldsThisRound(){ return this.fieldsThisRound;}
    public Route getRoute(){ return this.route;}
    public void setRoute(Route route){ this.route = route;}
    /**
    *Konstruktor für ein Auto-Objekt
    *@param s Die Fahrbahn, die zugewiesen wird.
    *@param f Der Feld-Index, der zugewiesen wird.
    *@param lt Die Lebenszeit, die zugewiesen wird.
    *
    */
    public Auto(Fahrbahn s, int f, int lt){
        this.street = s;
        this.field = f;
        this.lifeTime = lt;

    }

    public void logStatus(){

        this.log[this.logIndex] = fieldsThisRound;
        this.logIndex++;
        fieldsThisRound = 0;
    }

    public double[] getLog(){
        return this.log;
    }

    /**
    *Helfermethode für Update. Gibt wahr zurück, wenn die Straße zu einer Ampel mündet
    *und die Ampel grün anzeigt oder wenn dort keine Ampel ist.
    *@return boolean Wahr, wenn gefahren werden darf.
    *@param street Die Straße, die geprüft werden soll.
    */
    public static boolean noRedLight(Fahrbahn street){

        if(street.getEnd() instanceof LightJunction){
            return street.getGreenPhase();
        }
        return !(street.getEnd() instanceof LightJunction);
    }

    /**
    *Helfermethode für Update. Prüft, ob das übegebene Feld auf dem Straßenfeld frei ist.
    *@return boolean Wahr, wenn es frei ist.
    *@param fields Boolean-Array der Straße, die es enthält.
    *@param nextField Integer, der den Index auf der Straße darstellt.
    */
    public static boolean isFree(boolean[] fields, int nextField){
        if(fields[nextField] == false) return true;
        return false;
    }

    /**
    *Helfermethode für Update. Findet den Index der Straße, die auf dem nächsten
    *Routensegment erwähnt wird.
    *@return int Index der Straße, die gefunden wurde.
    *@param net Initialisiertes Straßennetz
    *@param route Route eines Autos
    */
    public static int findIndex(Strassennetz net, Route route){
        for(int i = 0; i<net.getStreets().length; i++){
            if(net.getStreet(i).getStart() == route.getSegment() && net.getStreet(i).getEnd() == route.getNext().getSegment()){
                return i;
            }
        }
        return -1;
    }

    public double countWholeLog(){
        double result = 0.0;
        for(int j = 0;j<this.log.length; j++){
            result += this.log[j];
        }
        return result;
    }



    /**
    *Die Update-Methode von Auto. Das Auto bewegt sich auf der Straße vorwärts. Wenn
    * die Straße zu Ende ist, wird die nächste Straße auf der Route gefunden und das
    * Auto wechselt auf diese Straße, wenn sie frei ist und keine rote Ampel leuchtet.
    *@return nothing
    *@param net Initialisiertes Straßennetz
    */
    public void update(Strassennetz net){

            if(this.street.getFields().length-1 <= this.field){ //wenn am Ende der Straße
                if(this.route.getNext() != null){ //wenn es einen nächsten Routenpunkt gibt

                    //überprüfe ob die Ampel grün ist
                    if( noRedLight(this.street) ){

                        int j = findIndex(net, this.route);

                        if(isFree(net.getStreet(j).getFields(), 0) == true){
                            this.street.setFields(this.field, false);
                            this.street = net.getStreet(j);
                            this.route = this.route.getNext();
                            fieldsThisRound += 1.0;

                            this.field = 0;
                            this.street.setFields(this.field, true);
                        } else {
                            this.street.setFields(this.field, true);

                        }
                    }

                } else if(this.street.getFields().length-1 == this.field){
                    this.street.setFields(this.field, false);
                    this.field = -1; //das Auto ist außerhalb der Map



                }
            } else if(this.field != -1 && this.lifeTime > 0 && isFree(this.street.getFields(), this.field+1) == true){
                    this.street.setFields(this.field, false);
                    this.street.setFields(this.field+1, true);
                    this.field++;
                    this.fieldsThisRound += 1.0;
            } else {

                if(this.field != -1){
                    this.street.setFields(this.field, true);
                }

            }
            this.lifeTime++;
    }


}
