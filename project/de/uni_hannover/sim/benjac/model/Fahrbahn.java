package de.uni_hannover.sim.benjac.model;
import de.uni_hannover.sim.benjac.simulation.*;
import de.uni_hannover.sim.benjac.model.profiling.*;
/**
*Ein Straßen-Objekt, das Referenzen auf Start- und Endkreuzungen, die Länge der Straße,
*ein Feld, das anzeigt, wo sich Autos befinden, und eine Anzeige, ob die Ampelphase rot oder grün ist.
*@author benjac
*@version 1.0
*/
public class Fahrbahn implements DataSource{

    private Kreuzung start;
    private Kreuzung end;
    /**
    *Die Länge der Straße
    */
    private int length;
    /**
    *Ein boolean-Array, das anzeigt, wo auf der Straße sich Autos befinden. Wenn ein Feld
    *"wahr" anzeigt, befindet sich dort ein Auto.
    */
    private boolean[] fields;
    /**
    *Ein boolean, der anzeigt, ob die Straße Grünphase hat.
    */
    private boolean greenPhase = false;
    /**
    *Konstruktor für Straßen
    */

    private double[] log = new double[100];
    private int logIndex = 0;

    public Kreuzung getStart(){ return this.start;}
    public void setStart(Kreuzung start){ this.start = start;}

    public Kreuzung getEnd(){ return this.end;}
    public void setEnd(Kreuzung end){ this.end = end;}

    public int getLength(){ return this.length;}
    public void setLength(int length){ this.length = length;}

    public boolean[] getFields(){ return this.fields;}
    public void setFields(boolean[] fields){ this.fields = fields;}
    public void setFields(int index, boolean value){ this.fields[index] = value;}

    public boolean getGreenPhase(){ return this.greenPhase;}
    public void setGreenPhase(boolean value){ this.greenPhase = value;}



    double countCars(){
        double counter = 0.0;
        for(int j = 0; j<this.fields.length; j++){
            if(fields[j] == true) counter += 1.0;
        }
        return counter;
    }

    public void logStatus(){

        this.log[this.logIndex] = countCars();
        logIndex++;
    }

    public double[] getLog(){
        return this.log;
    }
    public void setLog(double[] log){ this.log = log;}

    public Fahrbahn(Kreuzung s, Kreuzung e, int l){
        this.start = s;
        this.end = e;
        this.length = l;
    }

}
