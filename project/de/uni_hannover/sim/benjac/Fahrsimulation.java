package de.uni_hannover.sim.benjac;
import de.uni_hannover.sim.benjac.simulation.*;
import de.uni_hannover.sim.benjac.model.*;
/**
*Klasse, die ein Straßennetz initialisiert und darauf eine Simulation durchführt.
*@author benjac
*@version 1.0
*/
public class Fahrsimulation{



    /**
    *Main-Funktion, das die run-Methode aus der Klasse Simulation mit dem initialisierten
    *Straßennetz aufruft.
    *@return nothing
    *@param args Eingabe über Terminal (wird als String-Array interpretiert)
    */
    public static void main(String[] args) {
        long seed = -1;
        int spawnProb = 20;
        int rounds = 20;
        boolean ifStreets = false;
        boolean ifCars = false;
        boolean ifLights = false;
        boolean ifAverageOfWay = false;
        if(args.length != 0){
            for(int n = 0; n<args.length; n++){

                if(args[n].equals("-duration") && n+1 != args.length){
                    rounds = Integer.parseInt(args[n+1]); n++;
                    continue;

                } else if(args[n].equals("-spawnrate-set") && n+1 != args.length){
                    if(Integer.parseInt(args[n+1]) >= 0 && Integer.parseInt(args[n+1]) <= 100){
                        spawnProb = Integer.parseInt(args[n+1]); n++;
                        continue;
                    } else {
                        printHelp();
                    }

                } else if(args[n].equals("-seed") && n+1 != args.length ) {
                    seed = Long.parseLong(args[n+1]); n++;
                    continue;

                } else if(args[n].equals("-inspect"))  {
                    for(int i=0; i<1; i++){
                        if(n+1 != args.length){
                            switch(addToInspect(args[n+1])){
                                case 1: ifStreets = true; break;
                                case 2: ifStreets = true; i=-1; n++; break;
                                case 3: ifLights = true; break;
                                case 4: ifLights = true; i=-1; n++;break;
                                case 5: ifCars = true; break;
                                case 6: ifCars = true; i=-1; n++; break;
                                case -1: printHelp();
                            }
                        } else printHelp();
                    }
                    n++;
                    continue;

                } else if(args[n].equals("-profile-cars")) {
                    ifAverageOfWay = true;
                    continue;

                } else {
                    printHelp();
                }
            }
        } else printHelp();



        Strassennetz net = initNet(rounds, spawnProb);
        net.setSeed(seed);
        Simulation.run(net, rounds, ifCars, ifStreets, ifLights, ifAverageOfWay);

    }

    private void printHelp(){
        System.out.println(
        "Commands for Simulation (all optional):\n\n"+
        "-seed <value>               Sets seed for random number generation\n"+
        "-duration <value>           Sets number of rounds for simulation (default: 20)\n"+
        "-inspect <name1>, <name2>   Simulation shows data for given Actors every round and shows average value\n"+
        "                             and standard deviation at the end. Possible Actors: Lights, Cars, Streets\n"+
        "-profile-cars               At the end of simulation average waytime for all cars that have completed\n"+
        "                             their way through the system is shown.\n"+
        "-spawnrate-set <value>      Sets spawn rate in per cent for all car spawn points in the system. \n"+
        "                             Allowed: integer values between 0 and 100. Default is 20%"
        );
        System.exit(0);
    }

    private static int addToInspect(String in){

        if(in.equals("street") || in.equals("Street") || in.equals("streets") || in.equals("Streets")){
            return 1;
        }
        if(in.equals("street,") || in.equals("Street,") || in.equals("streets,") || in.equals("Streets,")){
            return 2;
        }

        if(in.equals("light") || in.equals("lights") || in.equals("Light") || in.equals("Lights")){
            return 3;
        }
        if(in.equals("light,") || in.equals("lights,") || in.equals("Light,") || in.equals("Lights,")){
            return 4;
        }

        if(in.equals("car") || in.equals("cars") || in.equals("Car") || in.equals("Cars")){
            return 5;
        }
        if(in.equals("car,") || in.equals("cars,") || in.equals("Car,") || in.equals("Cars,")){
            return 6;
        }

        return -1;
    }

    /**
    *Erstellt ein Array aus Routen aus einem gegebenen Kreuzungs-Array und Indizes auf die ensprechenden Kreuzungen
    * innerhalb des Arrays.
    *@return Route[] Routen-Array
    *@param crossings Array aus Kreuzungen, aus dem sich dir Routen formen.
    *@param start1 Bis end2 Indizes auf Kreuzungen, die zur Route hinzugefügt werden sollen.

    */
    private static Route[] createRouteArr(Kreuzung[] crossings, int start1, int end1, int start2, int end2, int start3, int end3){
        Route r1 = new Route(crossings[start1]);
        Route r2 = new Route(crossings[end1]);
        r1.setNext(r2);

        Route s1 = new Route(crossings[start2]);
        Route s2 = new Route(crossings[end2]);
        s1.setNext(s2);

        Route t1 = new Route(crossings[start3]);
        Route t2 = new Route(crossings[end3]);
        t1.setNext(t2);



        Route[] routes = new Route[3];
        routes[0] = r1;
        routes[1] = s1;
        routes[2] = t1;

        return routes;
    }

    /**
    *Initialsiert ein Straßennetz und erzeugt Routenarrays für Systemgrenzen.
    *@return Strassennetz Initialisiertes Straßennetz
    *
    */
    private static Strassennetz initNet(int rounds, int spawnProb){
        // initialize Strassennetz
        //erzeuge Kreuzungen
        Border A = new Border("A");
        Border B = new Border("B");
        LightJunction C = new LightJunction("C");
        C.setLog(new double[rounds+1]);

        Border D = new Border("D");
        Border E = new Border("E");


        Kreuzung[] crossings = new Kreuzung[5];
        crossings[0] = A;
        crossings[1] = B;
        crossings[2] = C;
        crossings[3] = D;
        crossings[4] = E;

        //erzeuge Routen-Arrays für die grenzenmarkierenden Kreuzungen

        A.setRoutes(createRouteArr(crossings, 2, 4, 2, 3, 2, 1));
        B.setRoutes(createRouteArr(crossings, 2, 4, 2, 3, 2, 0));
        D.setRoutes(createRouteArr(crossings, 2, 1, 2, 0, 2, 4));
        E.setRoutes(createRouteArr(crossings, 2, 1, 2, 0, 2, 3));

        A.setSpawnProb(spawnProb);
        B.setSpawnProb(spawnProb);
        D.setSpawnProb(spawnProb);
        E.setSpawnProb(spawnProb);

        //erzeuge Straßen

        boolean[] northField = new boolean[7];
        Fahrbahn north1 = new Fahrbahn(A, C, 7);
        north1.setFields(northField);

        boolean[] westField = new boolean[4];
        Fahrbahn west1 = new Fahrbahn(B, C, 4);
        west1.setFields(westField);

        boolean[] eastField = new boolean[5];
        Fahrbahn east1 = new Fahrbahn(C, D, 5);
        east1.setFields(eastField);

        boolean[] southField = new boolean[3];
        Fahrbahn south1 = new Fahrbahn(C, E, 3);
        south1.setFields(southField);

        boolean[] northField2 = new boolean[7];
        Fahrbahn north2 = new Fahrbahn(C, A, 7);
        north2.setFields(northField2);

        boolean[] westField2 = new boolean[4];
        Fahrbahn west2 = new Fahrbahn(C, B, 4);
        west2.setFields(westField2);

        boolean[] eastField2 = new boolean[5];
        Fahrbahn east2 = new Fahrbahn(D, C, 5);
        east2.setFields(eastField2);

        boolean[] southField2 = new boolean[3];
        Fahrbahn south2 = new Fahrbahn(E, C, 3);
        south2.setFields(southField2);

        //füge Straßen an Kreuzungen an
        A.setOut(2,north1);
        A.setIn(2,north2);

        B.setOut(1,west1);
        B.setIn(1,west2);

        C.setIn(0,north1);
        C.setOut(0,north2);
        C.setOut(1,east1);
        C.setIn(1,east2);
        C.setOut(2,south1);
        C.setIn(2,south2);
        C.setIn(3,west1);
        C.setOut(3,west2);

        D.setIn(3,east1);
        D.setOut(3,east2);

        E.setIn(0,south1);
        E.setOut(0,south2);


        Strassennetz net = new Strassennetz(crossings, north1, east1, south1, west1, north2, east2, south2, west2);
        for(int n = 0; n<net.getStreets().length; n++){
            net.getStreet(n).setLog(new double[rounds+1]);
        }

        return net;
    }



}
