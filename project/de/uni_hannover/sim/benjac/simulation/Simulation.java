package de.uni_hannover.sim.benjac.simulation;
import de.uni_hannover.sim.benjac.model.*;
import de.uni_hannover.sim.benjac.model.profiling.*;
import de.uni_hannover.sim.benjac.simulation.*;
/**
*Eine Klasse, die eine Simulation für ein Straßennetz durchführt und laufend pro Runde
*das Straßennetz ausgibt. Dabei werden die Akteure geupdatet und somit Ampelphasen umgeschaltet,
*Autos bewegt und neue Autos gespawnt.
*@author benjac
*@version 1.0
*/
public class Simulation {


    /**
    *Gibt eine Straße formatiert mit Zeilenumbruch aus.
    * @return nothing
    *@param street Straße, die ausgegeben werden soll.
    */
    private static void printlnFahrbahn(Fahrbahn street){
        if(street.getEnd() instanceof LightJunction){
            if(street.getGreenPhase()){
                System.out.print("Ampel grün  ");
            } else {
                System.out.print("Ampel rot   ");
            }
        } else {
            System.out.print("keine Ampel ");
        }

        for(int k= 0; k<street.getLength(); k++){
            if(street.getFields()[k] == false){
                System.out.print("- ");
            } else {
                System.out.print("X ");
            }
        }
        System.out.print("\n");
    }

    /**
    *Helferfunktion für printlnAuto, die den Straßennamen bestimmt.
    *@return String Name der Straße
    *@param start Startkreuzung
    *@param end Endkreuzung
    */
    private static String getStreetName(String start, String end){
        if(start.equals("A") && end.equals("C")) return "Nord1";
        if(start.equals("C") && end.equals("D")) return "Ost1 ";
        if(start.equals("C") && end.equals("E")) return "Süd1 ";
        if(start.equals("B") && end.equals("C")) return "West1";
        if(start.equals("C") && end.equals("A")) return "Nord2";
        if(start.equals("D") && end.equals("C")) return "Ost2 ";
        if(start.equals("E") && end.equals("C")) return "Süd2 ";
        if(start.equals("C") && end.equals("B")) return "West2";

        return " ";
    }

    /**
    *Gibt ein Auto formatiert mit Zeilenumbruch aus.
    *@return nothing
    *@param net Initialisiertes Straßennetz
    *@param autoNum Index des Autos innerhalb des Akteur-Arrays des Netzes
    */
    private static void printlnAuto(Strassennetz net, int autoNum){
        System.out.print("Auto "+(autoNum+-4)+": "+"Strasse "+getStreetName(((Auto)net.getActor(autoNum)).getStreet().getStart().getName(), ((Auto)net.getActor(autoNum)).getStreet().getEnd().getName())+", Feld: "+ (((Auto)net.getActor(autoNum)).getField()+1));
        System.out.println(", Lebenszeit: "+((Auto)net.getActor(autoNum)).getLifeTime()+", Von: "+((Auto)net.getActor(autoNum)).getStreet().getStart().getName()+", Richtung: "+((Auto)net.getActor(autoNum)).getStreet().getEnd().getName());
    }

    /**
    *Gibt die Straßen des Netzes formatiert aus.
    *@return nothing
    *@param net Initialisiertes Straßennetz
    */
    private static void printStreets(Strassennetz net){
        System.out.println("\nFeld Nr.:\t\t   1 2 3 4 5 6 7");
        System.out.print("Strasse Nord1: ");
        printlnFahrbahn(net.getStreet(0));
        System.out.print("Strasse Ost1 : ");
        printlnFahrbahn(net.getStreet(1));
        System.out.print("Strasse Süd1 : ");
        printlnFahrbahn(net.getStreet(2));
        System.out.print("Strasse West1: ");
        printlnFahrbahn(net.getStreet(3));
        System.out.print("Strasse Nord2: ");
        printlnFahrbahn(net.getStreet(4));
        System.out.print("Strasse Ost2 : ");
        printlnFahrbahn(net.getStreet(5));
        System.out.print("Strasse Süd2 : ");
        printlnFahrbahn(net.getStreet(6));
        System.out.print("Strasse West2: ");
        printlnFahrbahn(net.getStreet(7));
    }

    /**
    *Führt Simulation durch für eine gewisse Anzahl an Runden. Erzeugt Autos, schaltet Ampelphasen um
    * und lässt Autos sich bewegen.
    *@return nothing
    *@param net Initialisiertes Straßennetz
    */
    public static void run(Strassennetz net, int rounds, boolean ifCars, boolean ifStreets, boolean ifLights, boolean ifAverageOfWay){
        int i = 0;
        while(i<=rounds){
            ///////////////Update Status der Akteure des Netzes

            for(int n = 0; n<net.getActors().length; n++) {
                if( net.getActor(n) instanceof Auto){
                    if(((Auto)net.getActor(n)).getField() == -1){

                        net.setWayCount(net.getWayCount() + ((Auto)net.getActor(n)).countWholeLog());
                        net.setCarsSoFar(net.getCarsSoFar()+1);
                        net.setActor(n, null);
                    }
                }
                if( net.getActor(n) != null ){
                    net.getActor(n).update(net);
                }
            }

            ///////////////Log Data Sources

            for(int n = 0; n<net.getActors().length; n++) {
                if(net.getActor(n) instanceof DataSource)
                    ((DataSource)net.getActor(n)).logStatus();
            }

            for(int n = 0; n<net.getStreets().length; n++) {
                net.getStreet(n).logStatus();
            }

            //////// Gebe aus:

            System.out.println("----------------------------------------------------------");
            System.out.print("\n");
            System.out.println("Runde Nr."+i);

            //Gebe actors aus
            if(ifCars){
                for(int k = 5; k<net.getActors().length; k++){
                    if(net.getActor(k) != null) printlnAuto(net, k);
                }
            }

            if(ifStreets) printStreets(net);

            System.out.println("_________________________________________________________");
            System.out.print("\n\n");

            i++;
        }
        if(ifAverageOfWay || ifLights || ifStreets) System.out.println("End stats: ");
        if(ifAverageOfWay){
            System.out.println("Average Way per Car:\t" +(net.getWayCount() /net.getCarsSoFar()));
        }
        if(ifLights){
            System.out.println("Traffic Light:\t\t" + "Average Waiting Cars per round: "+ Statistics.calculateMean(((LightJunction)net.getActor(2)).getLog())+
            " Standard Deviation: "+ Statistics.calculateStandardDeviation(((LightJunction)net.getActor(2)).getLog()));
        }
        if(ifStreets){
            for(int n = 0; n<net.getStreets().length; n++){
                System.out.println("Street "+getStreetName(net.getStreet(n).getStart().getName(), net.getStreet(n).getEnd().getName())+ ":\t\t" + "Average Cars per round: "+ Statistics.calculateMean(net.getStreet(n).getLog())+
                " Standard Deviation: "+ Statistics.calculateStandardDeviation(net.getStreet(n).getLog()));
            }
        }
    }
}
